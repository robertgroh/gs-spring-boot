package com.example.springboot;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class FibonacciController {

    /**
     * This method calculates the Fibonacci number for the specified position.
     *
     * @param position the position of the Fibonacci number to be calculated
     * @return the Fibonacci number at the specified position
     * @throws ResponseStatusException if the position is not a positive integer
     */
    @GetMapping("/fibonacci/{position}")
    public long getFibonacciNumber(@PathVariable int position) {
        // since we use a one-based index, anything below 1 is invalid
        if (position <= 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Position must be a positive integer.");
        }

        // return fast for positions 1 and 2, since they are the first two Fibonacci numbers
        if (position <= 2) {
            return 1;
        }

        // for positions 3 and above:
        // create an array as big as needed for the current task and initialize the first two numbers
        long[] fibonacci = new long[position];
        fibonacci[0] = 1;
        fibonacci[1] = 1;

        // calculate the rest of the Fibonacci numbers, till we reach the desired position
        for (int i = 2; i < position; i++) {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }

        // since the array is zero-based index, but position is one-based, we need to subtract 1 from the position
        return fibonacci[position - 1];
    }
}