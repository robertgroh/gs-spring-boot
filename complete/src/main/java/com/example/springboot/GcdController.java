package com.example.springboot;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class GcdController {

    /**
     * This method calculates the greatest common divisor (GCD) of two integers.
     *
     * @param requestBody The HTTP request body containing the two integers as key-value pairs with keys "val1" and "val2".
     * @return The greatest common divisor of the two input integers.
     */
    @PostMapping("/gcd")
    public int getGcd(@RequestBody Map<String, Integer> requestBody) {
        int val1 = requestBody.get("val1");
        int val2 = requestBody.get("val2");

        // take care for the case when either val1 or val2 is 0
        if (val1 == 0) {
            return Math.abs(val2);
        }
        if (val2 == 0) {
            return Math.abs(val1);
        }

        // use Euclidean algorithm to calculate the GCD of val1 and val2
        while (val2 != 0) {
            int remainder = val1 % val2;
            val1 = val2;
            val2 = remainder;
        }

        return Math.abs(val1);
    }
}