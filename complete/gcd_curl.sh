#!/bin/bash

NUMBER_ONE=-140
NUMBER_TWO=-42

json_body="{\"val1\": ${NUMBER_ONE}, \"val2\": ${NUMBER_TWO}}"

# Send a POST request to the endpoint with the JSON body
response=$(curl -s -H "Content-Type: application/json" -X POST -d "${json_body}" http://localhost:8080/gcd)

# Print the response
echo "Greatest common divisor: ${response}"