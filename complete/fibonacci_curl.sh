#!/bin/bash

POSITION=10

# Send the GET request to the endpoint
response=$(curl -s http://localhost:8080/fibonacci/${POSITION})

# Print the response
echo "Fibonacci number at position ${POSITION}: ${response}"